import { IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import React, { FC } from "react";
import { RouterProps } from "react-router-dom";
import WorkoutForm from "../components/WorkoutForm";

const CreateNewWorkout: FC<RouterProps> = ({ history }) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Create a new workout</IonTitle>
        </IonToolbar>
      </IonHeader>
      <WorkoutForm />
    </IonPage>
  );
};

export default CreateNewWorkout;

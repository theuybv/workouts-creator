import React from "react";
import {
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonReorder,
  IonReorderGroup,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import ExploreContainer from "../components/ExploreContainer";
import "./Tab1.css";

const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonList>
          <IonReorderGroup
            disabled={false}
            onIonItemReorder={({ detail }) => {
              detail.complete(true);
            }}
          >
            {/*-- Default reorder icon, end aligned items --*/}

            <IonItem>
              <IonLabel>Item 1</IonLabel>
              <IonReorder slot="end" />
            </IonItem>
            <IonItem>
              <IonLabel>Item 1</IonLabel>
              <IonReorder slot="end" />
            </IonItem>
            <IonItem>
              <IonLabel>Item 1</IonLabel>
              <IonReorder slot="end" />
            </IonItem>
          </IonReorderGroup>
        </IonList>
        <ExploreContainer name="Tab 1 page" />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;

import { gql } from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
  uuid: any;
};

export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type File = {
  __typename?: 'File';
  encoding: Scalars['String'];
  filename: Scalars['String'];
  id: Scalars['String'];
  mimetype: Scalars['String'];
  size: Scalars['Int'];
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  singleUpload: File;
};


export type MutationSingleUploadArgs = {
  file: Scalars['Upload'];
};

export type Query = {
  __typename?: 'Query';
  hello?: Maybe<Scalars['String']>;
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};


/** columns and relationships of "assets" */
export type Assets = {
  __typename?: 'assets';
  /** An object relationship */
  exercise?: Maybe<Exercises>;
  exercise_id?: Maybe<Scalars['uuid']>;
  /** An array relationship */
  exercises: Array<Exercises>;
  /** An array relationship */
  exercisesByVideoAssetId: Array<Exercises>;
  /** An aggregated array relationship */
  exercisesByVideoAssetId_aggregate: Exercises_Aggregate;
  /** An aggregated array relationship */
  exercises_aggregate: Exercises_Aggregate;
  id: Scalars['uuid'];
  order: Scalars['Int'];
  type?: Maybe<Scalars['String']>;
  url: Scalars['String'];
  /** An object relationship */
  workout?: Maybe<Workouts>;
  workout_id?: Maybe<Scalars['uuid']>;
  /** An array relationship */
  workouts: Array<Workouts>;
  /** An aggregated array relationship */
  workouts_aggregate: Workouts_Aggregate;
};


/** columns and relationships of "assets" */
export type AssetsExercisesArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** columns and relationships of "assets" */
export type AssetsExercisesByVideoAssetIdArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** columns and relationships of "assets" */
export type AssetsExercisesByVideoAssetId_AggregateArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** columns and relationships of "assets" */
export type AssetsExercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** columns and relationships of "assets" */
export type AssetsWorkoutsArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};


/** columns and relationships of "assets" */
export type AssetsWorkouts_AggregateArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};

/** aggregated selection of "assets" */
export type Assets_Aggregate = {
  __typename?: 'assets_aggregate';
  aggregate?: Maybe<Assets_Aggregate_Fields>;
  nodes: Array<Assets>;
};

/** aggregate fields of "assets" */
export type Assets_Aggregate_Fields = {
  __typename?: 'assets_aggregate_fields';
  avg?: Maybe<Assets_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Assets_Max_Fields>;
  min?: Maybe<Assets_Min_Fields>;
  stddev?: Maybe<Assets_Stddev_Fields>;
  stddev_pop?: Maybe<Assets_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Assets_Stddev_Samp_Fields>;
  sum?: Maybe<Assets_Sum_Fields>;
  var_pop?: Maybe<Assets_Var_Pop_Fields>;
  var_samp?: Maybe<Assets_Var_Samp_Fields>;
  variance?: Maybe<Assets_Variance_Fields>;
};


/** aggregate fields of "assets" */
export type Assets_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Assets_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "assets" */
export type Assets_Aggregate_Order_By = {
  avg?: Maybe<Assets_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Assets_Max_Order_By>;
  min?: Maybe<Assets_Min_Order_By>;
  stddev?: Maybe<Assets_Stddev_Order_By>;
  stddev_pop?: Maybe<Assets_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Assets_Stddev_Samp_Order_By>;
  sum?: Maybe<Assets_Sum_Order_By>;
  var_pop?: Maybe<Assets_Var_Pop_Order_By>;
  var_samp?: Maybe<Assets_Var_Samp_Order_By>;
  variance?: Maybe<Assets_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "assets" */
export type Assets_Arr_Rel_Insert_Input = {
  data: Array<Assets_Insert_Input>;
  on_conflict?: Maybe<Assets_On_Conflict>;
};

/** aggregate avg on columns */
export type Assets_Avg_Fields = {
  __typename?: 'assets_avg_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "assets" */
export type Assets_Avg_Order_By = {
  order?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "assets". All fields are combined with a logical 'AND'. */
export type Assets_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Assets_Bool_Exp>>>;
  _not?: Maybe<Assets_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Assets_Bool_Exp>>>;
  exercise?: Maybe<Exercises_Bool_Exp>;
  exercise_id?: Maybe<Uuid_Comparison_Exp>;
  exercises?: Maybe<Exercises_Bool_Exp>;
  exercisesByVideoAssetId?: Maybe<Exercises_Bool_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  order?: Maybe<Int_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
  url?: Maybe<String_Comparison_Exp>;
  workout?: Maybe<Workouts_Bool_Exp>;
  workout_id?: Maybe<Uuid_Comparison_Exp>;
  workouts?: Maybe<Workouts_Bool_Exp>;
};

/** unique or primary key constraints on table "assets" */
export enum Assets_Constraint {
  /** unique or primary key constraint */
  AssetsPkey = 'assets_pkey',
  /** unique or primary key constraint */
  AssetsUrlKey = 'assets_url_key'
}

/** input type for incrementing integer column in table "assets" */
export type Assets_Inc_Input = {
  order?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "assets" */
export type Assets_Insert_Input = {
  exercise?: Maybe<Exercises_Obj_Rel_Insert_Input>;
  exercise_id?: Maybe<Scalars['uuid']>;
  exercises?: Maybe<Exercises_Arr_Rel_Insert_Input>;
  exercisesByVideoAssetId?: Maybe<Exercises_Arr_Rel_Insert_Input>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  workout?: Maybe<Workouts_Obj_Rel_Insert_Input>;
  workout_id?: Maybe<Scalars['uuid']>;
  workouts?: Maybe<Workouts_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Assets_Max_Fields = {
  __typename?: 'assets_max_fields';
  exercise_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  workout_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "assets" */
export type Assets_Max_Order_By = {
  exercise_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  url?: Maybe<Order_By>;
  workout_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Assets_Min_Fields = {
  __typename?: 'assets_min_fields';
  exercise_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  workout_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "assets" */
export type Assets_Min_Order_By = {
  exercise_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  url?: Maybe<Order_By>;
  workout_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "assets" */
export type Assets_Mutation_Response = {
  __typename?: 'assets_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Assets>;
};

/** input type for inserting object relation for remote table "assets" */
export type Assets_Obj_Rel_Insert_Input = {
  data: Assets_Insert_Input;
  on_conflict?: Maybe<Assets_On_Conflict>;
};

/** on conflict condition type for table "assets" */
export type Assets_On_Conflict = {
  constraint: Assets_Constraint;
  update_columns: Array<Assets_Update_Column>;
  where?: Maybe<Assets_Bool_Exp>;
};

/** ordering options when selecting data from "assets" */
export type Assets_Order_By = {
  exercise?: Maybe<Exercises_Order_By>;
  exercise_id?: Maybe<Order_By>;
  exercisesByVideoAssetId_aggregate?: Maybe<Exercises_Aggregate_Order_By>;
  exercises_aggregate?: Maybe<Exercises_Aggregate_Order_By>;
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  url?: Maybe<Order_By>;
  workout?: Maybe<Workouts_Order_By>;
  workout_id?: Maybe<Order_By>;
  workouts_aggregate?: Maybe<Workouts_Aggregate_Order_By>;
};

/** primary key columns input for table: "assets" */
export type Assets_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "assets" */
export enum Assets_Select_Column {
  /** column name */
  ExerciseId = 'exercise_id',
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  Type = 'type',
  /** column name */
  Url = 'url',
  /** column name */
  WorkoutId = 'workout_id'
}

/** input type for updating data in table "assets" */
export type Assets_Set_Input = {
  exercise_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  workout_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type Assets_Stddev_Fields = {
  __typename?: 'assets_stddev_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "assets" */
export type Assets_Stddev_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Assets_Stddev_Pop_Fields = {
  __typename?: 'assets_stddev_pop_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "assets" */
export type Assets_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Assets_Stddev_Samp_Fields = {
  __typename?: 'assets_stddev_samp_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "assets" */
export type Assets_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Assets_Sum_Fields = {
  __typename?: 'assets_sum_fields';
  order?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "assets" */
export type Assets_Sum_Order_By = {
  order?: Maybe<Order_By>;
};

/** update columns of table "assets" */
export enum Assets_Update_Column {
  /** column name */
  ExerciseId = 'exercise_id',
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  Type = 'type',
  /** column name */
  Url = 'url',
  /** column name */
  WorkoutId = 'workout_id'
}

/** aggregate var_pop on columns */
export type Assets_Var_Pop_Fields = {
  __typename?: 'assets_var_pop_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "assets" */
export type Assets_Var_Pop_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Assets_Var_Samp_Fields = {
  __typename?: 'assets_var_samp_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "assets" */
export type Assets_Var_Samp_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Assets_Variance_Fields = {
  __typename?: 'assets_variance_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "assets" */
export type Assets_Variance_Order_By = {
  order?: Maybe<Order_By>;
};

/** columns and relationships of "exercise_types" */
export type Exercise_Types = {
  __typename?: 'exercise_types';
  name: Scalars['String'];
  /** An array relationship */
  round_exercises: Array<Round_Exercises>;
  /** An aggregated array relationship */
  round_exercises_aggregate: Round_Exercises_Aggregate;
};


/** columns and relationships of "exercise_types" */
export type Exercise_TypesRound_ExercisesArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** columns and relationships of "exercise_types" */
export type Exercise_TypesRound_Exercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};

/** aggregated selection of "exercise_types" */
export type Exercise_Types_Aggregate = {
  __typename?: 'exercise_types_aggregate';
  aggregate?: Maybe<Exercise_Types_Aggregate_Fields>;
  nodes: Array<Exercise_Types>;
};

/** aggregate fields of "exercise_types" */
export type Exercise_Types_Aggregate_Fields = {
  __typename?: 'exercise_types_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Exercise_Types_Max_Fields>;
  min?: Maybe<Exercise_Types_Min_Fields>;
};


/** aggregate fields of "exercise_types" */
export type Exercise_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Exercise_Types_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "exercise_types" */
export type Exercise_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Exercise_Types_Max_Order_By>;
  min?: Maybe<Exercise_Types_Min_Order_By>;
};

/** input type for inserting array relation for remote table "exercise_types" */
export type Exercise_Types_Arr_Rel_Insert_Input = {
  data: Array<Exercise_Types_Insert_Input>;
  on_conflict?: Maybe<Exercise_Types_On_Conflict>;
};

/** Boolean expression to filter rows from the table "exercise_types". All fields are combined with a logical 'AND'. */
export type Exercise_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Exercise_Types_Bool_Exp>>>;
  _not?: Maybe<Exercise_Types_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Exercise_Types_Bool_Exp>>>;
  name?: Maybe<String_Comparison_Exp>;
  round_exercises?: Maybe<Round_Exercises_Bool_Exp>;
};

/** unique or primary key constraints on table "exercise_types" */
export enum Exercise_Types_Constraint {
  /** unique or primary key constraint */
  ExerciseTypesPkey = 'exercise_types_pkey'
}

export enum Exercise_Types_Enum {
  Duration = 'DURATION',
  Repetition = 'REPETITION',
  Rest = 'REST'
}

/** expression to compare columns of type exercise_types_enum. All fields are combined with logical 'AND'. */
export type Exercise_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Exercise_Types_Enum>;
  _in?: Maybe<Array<Exercise_Types_Enum>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Exercise_Types_Enum>;
  _nin?: Maybe<Array<Exercise_Types_Enum>>;
};

/** input type for inserting data into table "exercise_types" */
export type Exercise_Types_Insert_Input = {
  name?: Maybe<Scalars['String']>;
  round_exercises?: Maybe<Round_Exercises_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Exercise_Types_Max_Fields = {
  __typename?: 'exercise_types_max_fields';
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "exercise_types" */
export type Exercise_Types_Max_Order_By = {
  name?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Exercise_Types_Min_Fields = {
  __typename?: 'exercise_types_min_fields';
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "exercise_types" */
export type Exercise_Types_Min_Order_By = {
  name?: Maybe<Order_By>;
};

/** response of any mutation on the table "exercise_types" */
export type Exercise_Types_Mutation_Response = {
  __typename?: 'exercise_types_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Exercise_Types>;
};

/** input type for inserting object relation for remote table "exercise_types" */
export type Exercise_Types_Obj_Rel_Insert_Input = {
  data: Exercise_Types_Insert_Input;
  on_conflict?: Maybe<Exercise_Types_On_Conflict>;
};

/** on conflict condition type for table "exercise_types" */
export type Exercise_Types_On_Conflict = {
  constraint: Exercise_Types_Constraint;
  update_columns: Array<Exercise_Types_Update_Column>;
  where?: Maybe<Exercise_Types_Bool_Exp>;
};

/** ordering options when selecting data from "exercise_types" */
export type Exercise_Types_Order_By = {
  name?: Maybe<Order_By>;
  round_exercises_aggregate?: Maybe<Round_Exercises_Aggregate_Order_By>;
};

/** primary key columns input for table: "exercise_types" */
export type Exercise_Types_Pk_Columns_Input = {
  name: Scalars['String'];
};

/** select columns of table "exercise_types" */
export enum Exercise_Types_Select_Column {
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "exercise_types" */
export type Exercise_Types_Set_Input = {
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "exercise_types" */
export enum Exercise_Types_Update_Column {
  /** column name */
  Name = 'name'
}

/** columns and relationships of "exercises" */
export type Exercises = {
  __typename?: 'exercises';
  /** An object relationship */
  asset?: Maybe<Assets>;
  /** An object relationship */
  assetByVideoAssetId?: Maybe<Assets>;
  /** An array relationship */
  assets: Array<Assets>;
  /** An aggregated array relationship */
  assets_aggregate: Assets_Aggregate;
  description?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  image_asset_id?: Maybe<Scalars['uuid']>;
  name: Scalars['String'];
  /** An array relationship */
  round_exercises: Array<Round_Exercises>;
  /** An aggregated array relationship */
  round_exercises_aggregate: Round_Exercises_Aggregate;
  video_asset_id?: Maybe<Scalars['uuid']>;
};


/** columns and relationships of "exercises" */
export type ExercisesAssetsArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** columns and relationships of "exercises" */
export type ExercisesAssets_AggregateArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** columns and relationships of "exercises" */
export type ExercisesRound_ExercisesArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** columns and relationships of "exercises" */
export type ExercisesRound_Exercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};

/** aggregated selection of "exercises" */
export type Exercises_Aggregate = {
  __typename?: 'exercises_aggregate';
  aggregate?: Maybe<Exercises_Aggregate_Fields>;
  nodes: Array<Exercises>;
};

/** aggregate fields of "exercises" */
export type Exercises_Aggregate_Fields = {
  __typename?: 'exercises_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Exercises_Max_Fields>;
  min?: Maybe<Exercises_Min_Fields>;
};


/** aggregate fields of "exercises" */
export type Exercises_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Exercises_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "exercises" */
export type Exercises_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Exercises_Max_Order_By>;
  min?: Maybe<Exercises_Min_Order_By>;
};

/** input type for inserting array relation for remote table "exercises" */
export type Exercises_Arr_Rel_Insert_Input = {
  data: Array<Exercises_Insert_Input>;
  on_conflict?: Maybe<Exercises_On_Conflict>;
};

/** Boolean expression to filter rows from the table "exercises". All fields are combined with a logical 'AND'. */
export type Exercises_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Exercises_Bool_Exp>>>;
  _not?: Maybe<Exercises_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Exercises_Bool_Exp>>>;
  asset?: Maybe<Assets_Bool_Exp>;
  assetByVideoAssetId?: Maybe<Assets_Bool_Exp>;
  assets?: Maybe<Assets_Bool_Exp>;
  description?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  image_asset_id?: Maybe<Uuid_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  round_exercises?: Maybe<Round_Exercises_Bool_Exp>;
  video_asset_id?: Maybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "exercises" */
export enum Exercises_Constraint {
  /** unique or primary key constraint */
  ExercisesNameKey = 'exercises_name_key',
  /** unique or primary key constraint */
  ExercisesPkey = 'exercises_pkey'
}

/** input type for inserting data into table "exercises" */
export type Exercises_Insert_Input = {
  asset?: Maybe<Assets_Obj_Rel_Insert_Input>;
  assetByVideoAssetId?: Maybe<Assets_Obj_Rel_Insert_Input>;
  assets?: Maybe<Assets_Arr_Rel_Insert_Input>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  round_exercises?: Maybe<Round_Exercises_Arr_Rel_Insert_Input>;
  video_asset_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Exercises_Max_Fields = {
  __typename?: 'exercises_max_fields';
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  video_asset_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "exercises" */
export type Exercises_Max_Order_By = {
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  image_asset_id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  video_asset_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Exercises_Min_Fields = {
  __typename?: 'exercises_min_fields';
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  video_asset_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "exercises" */
export type Exercises_Min_Order_By = {
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  image_asset_id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  video_asset_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "exercises" */
export type Exercises_Mutation_Response = {
  __typename?: 'exercises_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Exercises>;
};

/** input type for inserting object relation for remote table "exercises" */
export type Exercises_Obj_Rel_Insert_Input = {
  data: Exercises_Insert_Input;
  on_conflict?: Maybe<Exercises_On_Conflict>;
};

/** on conflict condition type for table "exercises" */
export type Exercises_On_Conflict = {
  constraint: Exercises_Constraint;
  update_columns: Array<Exercises_Update_Column>;
  where?: Maybe<Exercises_Bool_Exp>;
};

/** ordering options when selecting data from "exercises" */
export type Exercises_Order_By = {
  asset?: Maybe<Assets_Order_By>;
  assetByVideoAssetId?: Maybe<Assets_Order_By>;
  assets_aggregate?: Maybe<Assets_Aggregate_Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  image_asset_id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  round_exercises_aggregate?: Maybe<Round_Exercises_Aggregate_Order_By>;
  video_asset_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "exercises" */
export type Exercises_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "exercises" */
export enum Exercises_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  ImageAssetId = 'image_asset_id',
  /** column name */
  Name = 'name',
  /** column name */
  VideoAssetId = 'video_asset_id'
}

/** input type for updating data in table "exercises" */
export type Exercises_Set_Input = {
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  video_asset_id?: Maybe<Scalars['uuid']>;
};

/** update columns of table "exercises" */
export enum Exercises_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  ImageAssetId = 'image_asset_id',
  /** column name */
  Name = 'name',
  /** column name */
  VideoAssetId = 'video_asset_id'
}

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "assets" */
  delete_assets?: Maybe<Assets_Mutation_Response>;
  /** delete single row from the table: "assets" */
  delete_assets_by_pk?: Maybe<Assets>;
  /** delete data from the table: "exercise_types" */
  delete_exercise_types?: Maybe<Exercise_Types_Mutation_Response>;
  /** delete single row from the table: "exercise_types" */
  delete_exercise_types_by_pk?: Maybe<Exercise_Types>;
  /** delete data from the table: "exercises" */
  delete_exercises?: Maybe<Exercises_Mutation_Response>;
  /** delete single row from the table: "exercises" */
  delete_exercises_by_pk?: Maybe<Exercises>;
  /** delete data from the table: "round_exercises" */
  delete_round_exercises?: Maybe<Round_Exercises_Mutation_Response>;
  /** delete single row from the table: "round_exercises" */
  delete_round_exercises_by_pk?: Maybe<Round_Exercises>;
  /** delete data from the table: "rounds" */
  delete_rounds?: Maybe<Rounds_Mutation_Response>;
  /** delete single row from the table: "rounds" */
  delete_rounds_by_pk?: Maybe<Rounds>;
  /** delete data from the table: "workouts" */
  delete_workouts?: Maybe<Workouts_Mutation_Response>;
  /** delete single row from the table: "workouts" */
  delete_workouts_by_pk?: Maybe<Workouts>;
  /** insert data into the table: "assets" */
  insert_assets?: Maybe<Assets_Mutation_Response>;
  /** insert a single row into the table: "assets" */
  insert_assets_one?: Maybe<Assets>;
  /** insert data into the table: "exercise_types" */
  insert_exercise_types?: Maybe<Exercise_Types_Mutation_Response>;
  /** insert a single row into the table: "exercise_types" */
  insert_exercise_types_one?: Maybe<Exercise_Types>;
  /** insert data into the table: "exercises" */
  insert_exercises?: Maybe<Exercises_Mutation_Response>;
  /** insert a single row into the table: "exercises" */
  insert_exercises_one?: Maybe<Exercises>;
  /** insert data into the table: "round_exercises" */
  insert_round_exercises?: Maybe<Round_Exercises_Mutation_Response>;
  /** insert a single row into the table: "round_exercises" */
  insert_round_exercises_one?: Maybe<Round_Exercises>;
  /** insert data into the table: "rounds" */
  insert_rounds?: Maybe<Rounds_Mutation_Response>;
  /** insert a single row into the table: "rounds" */
  insert_rounds_one?: Maybe<Rounds>;
  /** insert data into the table: "workouts" */
  insert_workouts?: Maybe<Workouts_Mutation_Response>;
  /** insert a single row into the table: "workouts" */
  insert_workouts_one?: Maybe<Workouts>;
  singleUpload: File;
  /** update data of the table: "assets" */
  update_assets?: Maybe<Assets_Mutation_Response>;
  /** update single row of the table: "assets" */
  update_assets_by_pk?: Maybe<Assets>;
  /** update data of the table: "exercise_types" */
  update_exercise_types?: Maybe<Exercise_Types_Mutation_Response>;
  /** update single row of the table: "exercise_types" */
  update_exercise_types_by_pk?: Maybe<Exercise_Types>;
  /** update data of the table: "exercises" */
  update_exercises?: Maybe<Exercises_Mutation_Response>;
  /** update single row of the table: "exercises" */
  update_exercises_by_pk?: Maybe<Exercises>;
  /** update data of the table: "round_exercises" */
  update_round_exercises?: Maybe<Round_Exercises_Mutation_Response>;
  /** update single row of the table: "round_exercises" */
  update_round_exercises_by_pk?: Maybe<Round_Exercises>;
  /** update data of the table: "rounds" */
  update_rounds?: Maybe<Rounds_Mutation_Response>;
  /** update single row of the table: "rounds" */
  update_rounds_by_pk?: Maybe<Rounds>;
  /** update data of the table: "workouts" */
  update_workouts?: Maybe<Workouts_Mutation_Response>;
  /** update single row of the table: "workouts" */
  update_workouts_by_pk?: Maybe<Workouts>;
};


/** mutation root */
export type Mutation_RootDelete_AssetsArgs = {
  where: Assets_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Assets_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Exercise_TypesArgs = {
  where: Exercise_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Exercise_Types_By_PkArgs = {
  name: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_ExercisesArgs = {
  where: Exercises_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Exercises_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Round_ExercisesArgs = {
  where: Round_Exercises_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Round_Exercises_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_RoundsArgs = {
  where: Rounds_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Rounds_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_WorkoutsArgs = {
  where: Workouts_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Workouts_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootInsert_AssetsArgs = {
  objects: Array<Assets_Insert_Input>;
  on_conflict?: Maybe<Assets_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Assets_OneArgs = {
  object: Assets_Insert_Input;
  on_conflict?: Maybe<Assets_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Exercise_TypesArgs = {
  objects: Array<Exercise_Types_Insert_Input>;
  on_conflict?: Maybe<Exercise_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Exercise_Types_OneArgs = {
  object: Exercise_Types_Insert_Input;
  on_conflict?: Maybe<Exercise_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ExercisesArgs = {
  objects: Array<Exercises_Insert_Input>;
  on_conflict?: Maybe<Exercises_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Exercises_OneArgs = {
  object: Exercises_Insert_Input;
  on_conflict?: Maybe<Exercises_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Round_ExercisesArgs = {
  objects: Array<Round_Exercises_Insert_Input>;
  on_conflict?: Maybe<Round_Exercises_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Round_Exercises_OneArgs = {
  object: Round_Exercises_Insert_Input;
  on_conflict?: Maybe<Round_Exercises_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_RoundsArgs = {
  objects: Array<Rounds_Insert_Input>;
  on_conflict?: Maybe<Rounds_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rounds_OneArgs = {
  object: Rounds_Insert_Input;
  on_conflict?: Maybe<Rounds_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_WorkoutsArgs = {
  objects: Array<Workouts_Insert_Input>;
  on_conflict?: Maybe<Workouts_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Workouts_OneArgs = {
  object: Workouts_Insert_Input;
  on_conflict?: Maybe<Workouts_On_Conflict>;
};


/** mutation root */
export type Mutation_RootSingleUploadArgs = {
  file: Scalars['Upload'];
};


/** mutation root */
export type Mutation_RootUpdate_AssetsArgs = {
  _inc?: Maybe<Assets_Inc_Input>;
  _set?: Maybe<Assets_Set_Input>;
  where: Assets_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Assets_By_PkArgs = {
  _inc?: Maybe<Assets_Inc_Input>;
  _set?: Maybe<Assets_Set_Input>;
  pk_columns: Assets_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Exercise_TypesArgs = {
  _set?: Maybe<Exercise_Types_Set_Input>;
  where: Exercise_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Exercise_Types_By_PkArgs = {
  _set?: Maybe<Exercise_Types_Set_Input>;
  pk_columns: Exercise_Types_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ExercisesArgs = {
  _set?: Maybe<Exercises_Set_Input>;
  where: Exercises_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Exercises_By_PkArgs = {
  _set?: Maybe<Exercises_Set_Input>;
  pk_columns: Exercises_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Round_ExercisesArgs = {
  _inc?: Maybe<Round_Exercises_Inc_Input>;
  _set?: Maybe<Round_Exercises_Set_Input>;
  where: Round_Exercises_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Round_Exercises_By_PkArgs = {
  _inc?: Maybe<Round_Exercises_Inc_Input>;
  _set?: Maybe<Round_Exercises_Set_Input>;
  pk_columns: Round_Exercises_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_RoundsArgs = {
  _inc?: Maybe<Rounds_Inc_Input>;
  _set?: Maybe<Rounds_Set_Input>;
  where: Rounds_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Rounds_By_PkArgs = {
  _inc?: Maybe<Rounds_Inc_Input>;
  _set?: Maybe<Rounds_Set_Input>;
  pk_columns: Rounds_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_WorkoutsArgs = {
  _inc?: Maybe<Workouts_Inc_Input>;
  _set?: Maybe<Workouts_Set_Input>;
  where: Workouts_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Workouts_By_PkArgs = {
  _inc?: Maybe<Workouts_Inc_Input>;
  _set?: Maybe<Workouts_Set_Input>;
  pk_columns: Workouts_Pk_Columns_Input;
};

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** query root */
export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "assets" */
  assets: Array<Assets>;
  /** fetch aggregated fields from the table: "assets" */
  assets_aggregate: Assets_Aggregate;
  /** fetch data from the table: "assets" using primary key columns */
  assets_by_pk?: Maybe<Assets>;
  /** fetch data from the table: "exercise_types" */
  exercise_types: Array<Exercise_Types>;
  /** fetch aggregated fields from the table: "exercise_types" */
  exercise_types_aggregate: Exercise_Types_Aggregate;
  /** fetch data from the table: "exercise_types" using primary key columns */
  exercise_types_by_pk?: Maybe<Exercise_Types>;
  /** fetch data from the table: "exercises" */
  exercises: Array<Exercises>;
  /** fetch aggregated fields from the table: "exercises" */
  exercises_aggregate: Exercises_Aggregate;
  /** fetch data from the table: "exercises" using primary key columns */
  exercises_by_pk?: Maybe<Exercises>;
  hello?: Maybe<Scalars['String']>;
  /** fetch data from the table: "round_exercises" */
  round_exercises: Array<Round_Exercises>;
  /** fetch aggregated fields from the table: "round_exercises" */
  round_exercises_aggregate: Round_Exercises_Aggregate;
  /** fetch data from the table: "round_exercises" using primary key columns */
  round_exercises_by_pk?: Maybe<Round_Exercises>;
  /** fetch data from the table: "rounds" */
  rounds: Array<Rounds>;
  /** fetch aggregated fields from the table: "rounds" */
  rounds_aggregate: Rounds_Aggregate;
  /** fetch data from the table: "rounds" using primary key columns */
  rounds_by_pk?: Maybe<Rounds>;
  /** fetch data from the table: "workouts" */
  workouts: Array<Workouts>;
  /** fetch aggregated fields from the table: "workouts" */
  workouts_aggregate: Workouts_Aggregate;
  /** fetch data from the table: "workouts" using primary key columns */
  workouts_by_pk?: Maybe<Workouts>;
};


/** query root */
export type Query_RootAssetsArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** query root */
export type Query_RootAssets_AggregateArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** query root */
export type Query_RootAssets_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootExercise_TypesArgs = {
  distinct_on?: Maybe<Array<Exercise_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercise_Types_Order_By>>;
  where?: Maybe<Exercise_Types_Bool_Exp>;
};


/** query root */
export type Query_RootExercise_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Exercise_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercise_Types_Order_By>>;
  where?: Maybe<Exercise_Types_Bool_Exp>;
};


/** query root */
export type Query_RootExercise_Types_By_PkArgs = {
  name: Scalars['String'];
};


/** query root */
export type Query_RootExercisesArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** query root */
export type Query_RootExercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** query root */
export type Query_RootExercises_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootRound_ExercisesArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** query root */
export type Query_RootRound_Exercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** query root */
export type Query_RootRound_Exercises_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootRoundsArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};


/** query root */
export type Query_RootRounds_AggregateArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};


/** query root */
export type Query_RootRounds_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootWorkoutsArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};


/** query root */
export type Query_RootWorkouts_AggregateArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};


/** query root */
export type Query_RootWorkouts_By_PkArgs = {
  id: Scalars['uuid'];
};

/**
 * A single exercise in a round with the meta information
 * 
 * 
 * columns and relationships of "round_exercises"
 */
export type Round_Exercises = {
  __typename?: 'round_exercises';
  /** An object relationship */
  exercise?: Maybe<Exercises>;
  exercise_id?: Maybe<Scalars['uuid']>;
  /** An object relationship */
  exercise_type: Exercise_Types;
  id: Scalars['uuid'];
  order: Scalars['Int'];
  /** An object relationship */
  round?: Maybe<Rounds>;
  round_id?: Maybe<Scalars['uuid']>;
  /** An array relationship */
  rounds: Array<Rounds>;
  /** An aggregated array relationship */
  rounds_aggregate: Rounds_Aggregate;
  type: Exercise_Types_Enum;
  value: Scalars['Int'];
};


/**
 * A single exercise in a round with the meta information
 * 
 * 
 * columns and relationships of "round_exercises"
 */
export type Round_ExercisesRoundsArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};


/**
 * A single exercise in a round with the meta information
 * 
 * 
 * columns and relationships of "round_exercises"
 */
export type Round_ExercisesRounds_AggregateArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};

/** aggregated selection of "round_exercises" */
export type Round_Exercises_Aggregate = {
  __typename?: 'round_exercises_aggregate';
  aggregate?: Maybe<Round_Exercises_Aggregate_Fields>;
  nodes: Array<Round_Exercises>;
};

/** aggregate fields of "round_exercises" */
export type Round_Exercises_Aggregate_Fields = {
  __typename?: 'round_exercises_aggregate_fields';
  avg?: Maybe<Round_Exercises_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Round_Exercises_Max_Fields>;
  min?: Maybe<Round_Exercises_Min_Fields>;
  stddev?: Maybe<Round_Exercises_Stddev_Fields>;
  stddev_pop?: Maybe<Round_Exercises_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Round_Exercises_Stddev_Samp_Fields>;
  sum?: Maybe<Round_Exercises_Sum_Fields>;
  var_pop?: Maybe<Round_Exercises_Var_Pop_Fields>;
  var_samp?: Maybe<Round_Exercises_Var_Samp_Fields>;
  variance?: Maybe<Round_Exercises_Variance_Fields>;
};


/** aggregate fields of "round_exercises" */
export type Round_Exercises_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Round_Exercises_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "round_exercises" */
export type Round_Exercises_Aggregate_Order_By = {
  avg?: Maybe<Round_Exercises_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Round_Exercises_Max_Order_By>;
  min?: Maybe<Round_Exercises_Min_Order_By>;
  stddev?: Maybe<Round_Exercises_Stddev_Order_By>;
  stddev_pop?: Maybe<Round_Exercises_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Round_Exercises_Stddev_Samp_Order_By>;
  sum?: Maybe<Round_Exercises_Sum_Order_By>;
  var_pop?: Maybe<Round_Exercises_Var_Pop_Order_By>;
  var_samp?: Maybe<Round_Exercises_Var_Samp_Order_By>;
  variance?: Maybe<Round_Exercises_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "round_exercises" */
export type Round_Exercises_Arr_Rel_Insert_Input = {
  data: Array<Round_Exercises_Insert_Input>;
  on_conflict?: Maybe<Round_Exercises_On_Conflict>;
};

/** aggregate avg on columns */
export type Round_Exercises_Avg_Fields = {
  __typename?: 'round_exercises_avg_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "round_exercises" */
export type Round_Exercises_Avg_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "round_exercises". All fields are combined with a logical 'AND'. */
export type Round_Exercises_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Round_Exercises_Bool_Exp>>>;
  _not?: Maybe<Round_Exercises_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Round_Exercises_Bool_Exp>>>;
  exercise?: Maybe<Exercises_Bool_Exp>;
  exercise_id?: Maybe<Uuid_Comparison_Exp>;
  exercise_type?: Maybe<Exercise_Types_Bool_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  order?: Maybe<Int_Comparison_Exp>;
  round?: Maybe<Rounds_Bool_Exp>;
  round_id?: Maybe<Uuid_Comparison_Exp>;
  rounds?: Maybe<Rounds_Bool_Exp>;
  type?: Maybe<Exercise_Types_Enum_Comparison_Exp>;
  value?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "round_exercises" */
export enum Round_Exercises_Constraint {
  /** unique or primary key constraint */
  RoundExercisesPkey = 'round_exercises_pkey'
}

/** input type for incrementing integer column in table "round_exercises" */
export type Round_Exercises_Inc_Input = {
  order?: Maybe<Scalars['Int']>;
  value?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "round_exercises" */
export type Round_Exercises_Insert_Input = {
  exercise?: Maybe<Exercises_Obj_Rel_Insert_Input>;
  exercise_id?: Maybe<Scalars['uuid']>;
  exercise_type?: Maybe<Exercise_Types_Obj_Rel_Insert_Input>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round?: Maybe<Rounds_Obj_Rel_Insert_Input>;
  round_id?: Maybe<Scalars['uuid']>;
  rounds?: Maybe<Rounds_Arr_Rel_Insert_Input>;
  type?: Maybe<Exercise_Types_Enum>;
  value?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type Round_Exercises_Max_Fields = {
  __typename?: 'round_exercises_max_fields';
  exercise_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_id?: Maybe<Scalars['uuid']>;
  value?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "round_exercises" */
export type Round_Exercises_Max_Order_By = {
  exercise_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  round_id?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Round_Exercises_Min_Fields = {
  __typename?: 'round_exercises_min_fields';
  exercise_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_id?: Maybe<Scalars['uuid']>;
  value?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "round_exercises" */
export type Round_Exercises_Min_Order_By = {
  exercise_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  round_id?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** response of any mutation on the table "round_exercises" */
export type Round_Exercises_Mutation_Response = {
  __typename?: 'round_exercises_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Round_Exercises>;
};

/** input type for inserting object relation for remote table "round_exercises" */
export type Round_Exercises_Obj_Rel_Insert_Input = {
  data: Round_Exercises_Insert_Input;
  on_conflict?: Maybe<Round_Exercises_On_Conflict>;
};

/** on conflict condition type for table "round_exercises" */
export type Round_Exercises_On_Conflict = {
  constraint: Round_Exercises_Constraint;
  update_columns: Array<Round_Exercises_Update_Column>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};

/** ordering options when selecting data from "round_exercises" */
export type Round_Exercises_Order_By = {
  exercise?: Maybe<Exercises_Order_By>;
  exercise_id?: Maybe<Order_By>;
  exercise_type?: Maybe<Exercise_Types_Order_By>;
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  round?: Maybe<Rounds_Order_By>;
  round_id?: Maybe<Order_By>;
  rounds_aggregate?: Maybe<Rounds_Aggregate_Order_By>;
  type?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** primary key columns input for table: "round_exercises" */
export type Round_Exercises_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "round_exercises" */
export enum Round_Exercises_Select_Column {
  /** column name */
  ExerciseId = 'exercise_id',
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  RoundId = 'round_id',
  /** column name */
  Type = 'type',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "round_exercises" */
export type Round_Exercises_Set_Input = {
  exercise_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_id?: Maybe<Scalars['uuid']>;
  type?: Maybe<Exercise_Types_Enum>;
  value?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type Round_Exercises_Stddev_Fields = {
  __typename?: 'round_exercises_stddev_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "round_exercises" */
export type Round_Exercises_Stddev_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Round_Exercises_Stddev_Pop_Fields = {
  __typename?: 'round_exercises_stddev_pop_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "round_exercises" */
export type Round_Exercises_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Round_Exercises_Stddev_Samp_Fields = {
  __typename?: 'round_exercises_stddev_samp_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "round_exercises" */
export type Round_Exercises_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Round_Exercises_Sum_Fields = {
  __typename?: 'round_exercises_sum_fields';
  order?: Maybe<Scalars['Int']>;
  value?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "round_exercises" */
export type Round_Exercises_Sum_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** update columns of table "round_exercises" */
export enum Round_Exercises_Update_Column {
  /** column name */
  ExerciseId = 'exercise_id',
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  RoundId = 'round_id',
  /** column name */
  Type = 'type',
  /** column name */
  Value = 'value'
}

/** aggregate var_pop on columns */
export type Round_Exercises_Var_Pop_Fields = {
  __typename?: 'round_exercises_var_pop_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "round_exercises" */
export type Round_Exercises_Var_Pop_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Round_Exercises_Var_Samp_Fields = {
  __typename?: 'round_exercises_var_samp_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "round_exercises" */
export type Round_Exercises_Var_Samp_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Round_Exercises_Variance_Fields = {
  __typename?: 'round_exercises_variance_fields';
  order?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "round_exercises" */
export type Round_Exercises_Variance_Order_By = {
  order?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** columns and relationships of "rounds" */
export type Rounds = {
  __typename?: 'rounds';
  id: Scalars['uuid'];
  order: Scalars['Int'];
  /** An object relationship */
  round_exercise?: Maybe<Round_Exercises>;
  round_exercise_id?: Maybe<Scalars['uuid']>;
  /** An array relationship */
  round_exercises: Array<Round_Exercises>;
  /** An aggregated array relationship */
  round_exercises_aggregate: Round_Exercises_Aggregate;
  /** An object relationship */
  workout?: Maybe<Workouts>;
  workout_id?: Maybe<Scalars['uuid']>;
  /** An array relationship */
  workouts: Array<Workouts>;
  /** An aggregated array relationship */
  workouts_aggregate: Workouts_Aggregate;
};


/** columns and relationships of "rounds" */
export type RoundsRound_ExercisesArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** columns and relationships of "rounds" */
export type RoundsRound_Exercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** columns and relationships of "rounds" */
export type RoundsWorkoutsArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};


/** columns and relationships of "rounds" */
export type RoundsWorkouts_AggregateArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};

/** aggregated selection of "rounds" */
export type Rounds_Aggregate = {
  __typename?: 'rounds_aggregate';
  aggregate?: Maybe<Rounds_Aggregate_Fields>;
  nodes: Array<Rounds>;
};

/** aggregate fields of "rounds" */
export type Rounds_Aggregate_Fields = {
  __typename?: 'rounds_aggregate_fields';
  avg?: Maybe<Rounds_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Rounds_Max_Fields>;
  min?: Maybe<Rounds_Min_Fields>;
  stddev?: Maybe<Rounds_Stddev_Fields>;
  stddev_pop?: Maybe<Rounds_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Rounds_Stddev_Samp_Fields>;
  sum?: Maybe<Rounds_Sum_Fields>;
  var_pop?: Maybe<Rounds_Var_Pop_Fields>;
  var_samp?: Maybe<Rounds_Var_Samp_Fields>;
  variance?: Maybe<Rounds_Variance_Fields>;
};


/** aggregate fields of "rounds" */
export type Rounds_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Rounds_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "rounds" */
export type Rounds_Aggregate_Order_By = {
  avg?: Maybe<Rounds_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Rounds_Max_Order_By>;
  min?: Maybe<Rounds_Min_Order_By>;
  stddev?: Maybe<Rounds_Stddev_Order_By>;
  stddev_pop?: Maybe<Rounds_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Rounds_Stddev_Samp_Order_By>;
  sum?: Maybe<Rounds_Sum_Order_By>;
  var_pop?: Maybe<Rounds_Var_Pop_Order_By>;
  var_samp?: Maybe<Rounds_Var_Samp_Order_By>;
  variance?: Maybe<Rounds_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "rounds" */
export type Rounds_Arr_Rel_Insert_Input = {
  data: Array<Rounds_Insert_Input>;
  on_conflict?: Maybe<Rounds_On_Conflict>;
};

/** aggregate avg on columns */
export type Rounds_Avg_Fields = {
  __typename?: 'rounds_avg_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "rounds" */
export type Rounds_Avg_Order_By = {
  order?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "rounds". All fields are combined with a logical 'AND'. */
export type Rounds_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Rounds_Bool_Exp>>>;
  _not?: Maybe<Rounds_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Rounds_Bool_Exp>>>;
  id?: Maybe<Uuid_Comparison_Exp>;
  order?: Maybe<Int_Comparison_Exp>;
  round_exercise?: Maybe<Round_Exercises_Bool_Exp>;
  round_exercise_id?: Maybe<Uuid_Comparison_Exp>;
  round_exercises?: Maybe<Round_Exercises_Bool_Exp>;
  workout?: Maybe<Workouts_Bool_Exp>;
  workout_id?: Maybe<Uuid_Comparison_Exp>;
  workouts?: Maybe<Workouts_Bool_Exp>;
};

/** unique or primary key constraints on table "rounds" */
export enum Rounds_Constraint {
  /** unique or primary key constraint */
  RoundsPkey = 'rounds_pkey'
}

/** input type for incrementing integer column in table "rounds" */
export type Rounds_Inc_Input = {
  order?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "rounds" */
export type Rounds_Insert_Input = {
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_exercise?: Maybe<Round_Exercises_Obj_Rel_Insert_Input>;
  round_exercise_id?: Maybe<Scalars['uuid']>;
  round_exercises?: Maybe<Round_Exercises_Arr_Rel_Insert_Input>;
  workout?: Maybe<Workouts_Obj_Rel_Insert_Input>;
  workout_id?: Maybe<Scalars['uuid']>;
  workouts?: Maybe<Workouts_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Rounds_Max_Fields = {
  __typename?: 'rounds_max_fields';
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_exercise_id?: Maybe<Scalars['uuid']>;
  workout_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "rounds" */
export type Rounds_Max_Order_By = {
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  round_exercise_id?: Maybe<Order_By>;
  workout_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Rounds_Min_Fields = {
  __typename?: 'rounds_min_fields';
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_exercise_id?: Maybe<Scalars['uuid']>;
  workout_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "rounds" */
export type Rounds_Min_Order_By = {
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  round_exercise_id?: Maybe<Order_By>;
  workout_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "rounds" */
export type Rounds_Mutation_Response = {
  __typename?: 'rounds_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Rounds>;
};

/** input type for inserting object relation for remote table "rounds" */
export type Rounds_Obj_Rel_Insert_Input = {
  data: Rounds_Insert_Input;
  on_conflict?: Maybe<Rounds_On_Conflict>;
};

/** on conflict condition type for table "rounds" */
export type Rounds_On_Conflict = {
  constraint: Rounds_Constraint;
  update_columns: Array<Rounds_Update_Column>;
  where?: Maybe<Rounds_Bool_Exp>;
};

/** ordering options when selecting data from "rounds" */
export type Rounds_Order_By = {
  id?: Maybe<Order_By>;
  order?: Maybe<Order_By>;
  round_exercise?: Maybe<Round_Exercises_Order_By>;
  round_exercise_id?: Maybe<Order_By>;
  round_exercises_aggregate?: Maybe<Round_Exercises_Aggregate_Order_By>;
  workout?: Maybe<Workouts_Order_By>;
  workout_id?: Maybe<Order_By>;
  workouts_aggregate?: Maybe<Workouts_Aggregate_Order_By>;
};

/** primary key columns input for table: "rounds" */
export type Rounds_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "rounds" */
export enum Rounds_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  RoundExerciseId = 'round_exercise_id',
  /** column name */
  WorkoutId = 'workout_id'
}

/** input type for updating data in table "rounds" */
export type Rounds_Set_Input = {
  id?: Maybe<Scalars['uuid']>;
  order?: Maybe<Scalars['Int']>;
  round_exercise_id?: Maybe<Scalars['uuid']>;
  workout_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type Rounds_Stddev_Fields = {
  __typename?: 'rounds_stddev_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "rounds" */
export type Rounds_Stddev_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Rounds_Stddev_Pop_Fields = {
  __typename?: 'rounds_stddev_pop_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "rounds" */
export type Rounds_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Rounds_Stddev_Samp_Fields = {
  __typename?: 'rounds_stddev_samp_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "rounds" */
export type Rounds_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Rounds_Sum_Fields = {
  __typename?: 'rounds_sum_fields';
  order?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "rounds" */
export type Rounds_Sum_Order_By = {
  order?: Maybe<Order_By>;
};

/** update columns of table "rounds" */
export enum Rounds_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  RoundExerciseId = 'round_exercise_id',
  /** column name */
  WorkoutId = 'workout_id'
}

/** aggregate var_pop on columns */
export type Rounds_Var_Pop_Fields = {
  __typename?: 'rounds_var_pop_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "rounds" */
export type Rounds_Var_Pop_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Rounds_Var_Samp_Fields = {
  __typename?: 'rounds_var_samp_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "rounds" */
export type Rounds_Var_Samp_Order_By = {
  order?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Rounds_Variance_Fields = {
  __typename?: 'rounds_variance_fields';
  order?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "rounds" */
export type Rounds_Variance_Order_By = {
  order?: Maybe<Order_By>;
};

/** subscription root */
export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "assets" */
  assets: Array<Assets>;
  /** fetch aggregated fields from the table: "assets" */
  assets_aggregate: Assets_Aggregate;
  /** fetch data from the table: "assets" using primary key columns */
  assets_by_pk?: Maybe<Assets>;
  /** fetch data from the table: "exercise_types" */
  exercise_types: Array<Exercise_Types>;
  /** fetch aggregated fields from the table: "exercise_types" */
  exercise_types_aggregate: Exercise_Types_Aggregate;
  /** fetch data from the table: "exercise_types" using primary key columns */
  exercise_types_by_pk?: Maybe<Exercise_Types>;
  /** fetch data from the table: "exercises" */
  exercises: Array<Exercises>;
  /** fetch aggregated fields from the table: "exercises" */
  exercises_aggregate: Exercises_Aggregate;
  /** fetch data from the table: "exercises" using primary key columns */
  exercises_by_pk?: Maybe<Exercises>;
  /** fetch data from the table: "round_exercises" */
  round_exercises: Array<Round_Exercises>;
  /** fetch aggregated fields from the table: "round_exercises" */
  round_exercises_aggregate: Round_Exercises_Aggregate;
  /** fetch data from the table: "round_exercises" using primary key columns */
  round_exercises_by_pk?: Maybe<Round_Exercises>;
  /** fetch data from the table: "rounds" */
  rounds: Array<Rounds>;
  /** fetch aggregated fields from the table: "rounds" */
  rounds_aggregate: Rounds_Aggregate;
  /** fetch data from the table: "rounds" using primary key columns */
  rounds_by_pk?: Maybe<Rounds>;
  /** fetch data from the table: "workouts" */
  workouts: Array<Workouts>;
  /** fetch aggregated fields from the table: "workouts" */
  workouts_aggregate: Workouts_Aggregate;
  /** fetch data from the table: "workouts" using primary key columns */
  workouts_by_pk?: Maybe<Workouts>;
};


/** subscription root */
export type Subscription_RootAssetsArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAssets_AggregateArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAssets_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootExercise_TypesArgs = {
  distinct_on?: Maybe<Array<Exercise_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercise_Types_Order_By>>;
  where?: Maybe<Exercise_Types_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootExercise_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Exercise_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercise_Types_Order_By>>;
  where?: Maybe<Exercise_Types_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootExercise_Types_By_PkArgs = {
  name: Scalars['String'];
};


/** subscription root */
export type Subscription_RootExercisesArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootExercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Exercises_Order_By>>;
  where?: Maybe<Exercises_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootExercises_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootRound_ExercisesArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRound_Exercises_AggregateArgs = {
  distinct_on?: Maybe<Array<Round_Exercises_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Round_Exercises_Order_By>>;
  where?: Maybe<Round_Exercises_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRound_Exercises_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootRoundsArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRounds_AggregateArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRounds_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootWorkoutsArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootWorkouts_AggregateArgs = {
  distinct_on?: Maybe<Array<Workouts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Workouts_Order_By>>;
  where?: Maybe<Workouts_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootWorkouts_By_PkArgs = {
  id: Scalars['uuid'];
};


/** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: Maybe<Scalars['uuid']>;
  _gt?: Maybe<Scalars['uuid']>;
  _gte?: Maybe<Scalars['uuid']>;
  _in?: Maybe<Array<Scalars['uuid']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['uuid']>;
  _lte?: Maybe<Scalars['uuid']>;
  _neq?: Maybe<Scalars['uuid']>;
  _nin?: Maybe<Array<Scalars['uuid']>>;
};

/** columns and relationships of "workouts" */
export type Workouts = {
  __typename?: 'workouts';
  /** An object relationship */
  asset?: Maybe<Assets>;
  /** An array relationship */
  assets: Array<Assets>;
  /** An aggregated array relationship */
  assets_aggregate: Assets_Aggregate;
  description?: Maybe<Scalars['String']>;
  difficulty: Scalars['Int'];
  duration: Scalars['Int'];
  id: Scalars['uuid'];
  image_asset_id?: Maybe<Scalars['uuid']>;
  name: Scalars['String'];
  /** An object relationship */
  round?: Maybe<Rounds>;
  round_id?: Maybe<Scalars['uuid']>;
  /** An array relationship */
  rounds: Array<Rounds>;
  /** An aggregated array relationship */
  rounds_aggregate: Rounds_Aggregate;
};


/** columns and relationships of "workouts" */
export type WorkoutsAssetsArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** columns and relationships of "workouts" */
export type WorkoutsAssets_AggregateArgs = {
  distinct_on?: Maybe<Array<Assets_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Assets_Order_By>>;
  where?: Maybe<Assets_Bool_Exp>;
};


/** columns and relationships of "workouts" */
export type WorkoutsRoundsArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};


/** columns and relationships of "workouts" */
export type WorkoutsRounds_AggregateArgs = {
  distinct_on?: Maybe<Array<Rounds_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Rounds_Order_By>>;
  where?: Maybe<Rounds_Bool_Exp>;
};

/** aggregated selection of "workouts" */
export type Workouts_Aggregate = {
  __typename?: 'workouts_aggregate';
  aggregate?: Maybe<Workouts_Aggregate_Fields>;
  nodes: Array<Workouts>;
};

/** aggregate fields of "workouts" */
export type Workouts_Aggregate_Fields = {
  __typename?: 'workouts_aggregate_fields';
  avg?: Maybe<Workouts_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Workouts_Max_Fields>;
  min?: Maybe<Workouts_Min_Fields>;
  stddev?: Maybe<Workouts_Stddev_Fields>;
  stddev_pop?: Maybe<Workouts_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Workouts_Stddev_Samp_Fields>;
  sum?: Maybe<Workouts_Sum_Fields>;
  var_pop?: Maybe<Workouts_Var_Pop_Fields>;
  var_samp?: Maybe<Workouts_Var_Samp_Fields>;
  variance?: Maybe<Workouts_Variance_Fields>;
};


/** aggregate fields of "workouts" */
export type Workouts_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Workouts_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "workouts" */
export type Workouts_Aggregate_Order_By = {
  avg?: Maybe<Workouts_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Workouts_Max_Order_By>;
  min?: Maybe<Workouts_Min_Order_By>;
  stddev?: Maybe<Workouts_Stddev_Order_By>;
  stddev_pop?: Maybe<Workouts_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Workouts_Stddev_Samp_Order_By>;
  sum?: Maybe<Workouts_Sum_Order_By>;
  var_pop?: Maybe<Workouts_Var_Pop_Order_By>;
  var_samp?: Maybe<Workouts_Var_Samp_Order_By>;
  variance?: Maybe<Workouts_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "workouts" */
export type Workouts_Arr_Rel_Insert_Input = {
  data: Array<Workouts_Insert_Input>;
  on_conflict?: Maybe<Workouts_On_Conflict>;
};

/** aggregate avg on columns */
export type Workouts_Avg_Fields = {
  __typename?: 'workouts_avg_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "workouts" */
export type Workouts_Avg_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "workouts". All fields are combined with a logical 'AND'. */
export type Workouts_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Workouts_Bool_Exp>>>;
  _not?: Maybe<Workouts_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Workouts_Bool_Exp>>>;
  asset?: Maybe<Assets_Bool_Exp>;
  assets?: Maybe<Assets_Bool_Exp>;
  description?: Maybe<String_Comparison_Exp>;
  difficulty?: Maybe<Int_Comparison_Exp>;
  duration?: Maybe<Int_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  image_asset_id?: Maybe<Uuid_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  round?: Maybe<Rounds_Bool_Exp>;
  round_id?: Maybe<Uuid_Comparison_Exp>;
  rounds?: Maybe<Rounds_Bool_Exp>;
};

/** unique or primary key constraints on table "workouts" */
export enum Workouts_Constraint {
  /** unique or primary key constraint */
  WorkoutsNameKey = 'workouts_name_key',
  /** unique or primary key constraint */
  WorkoutsPkey = 'workouts_pkey'
}

/** input type for incrementing integer column in table "workouts" */
export type Workouts_Inc_Input = {
  difficulty?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "workouts" */
export type Workouts_Insert_Input = {
  asset?: Maybe<Assets_Obj_Rel_Insert_Input>;
  assets?: Maybe<Assets_Arr_Rel_Insert_Input>;
  description?: Maybe<Scalars['String']>;
  difficulty?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  round?: Maybe<Rounds_Obj_Rel_Insert_Input>;
  round_id?: Maybe<Scalars['uuid']>;
  rounds?: Maybe<Rounds_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Workouts_Max_Fields = {
  __typename?: 'workouts_max_fields';
  description?: Maybe<Scalars['String']>;
  difficulty?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  round_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "workouts" */
export type Workouts_Max_Order_By = {
  description?: Maybe<Order_By>;
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  image_asset_id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  round_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Workouts_Min_Fields = {
  __typename?: 'workouts_min_fields';
  description?: Maybe<Scalars['String']>;
  difficulty?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  round_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "workouts" */
export type Workouts_Min_Order_By = {
  description?: Maybe<Order_By>;
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  image_asset_id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  round_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "workouts" */
export type Workouts_Mutation_Response = {
  __typename?: 'workouts_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Workouts>;
};

/** input type for inserting object relation for remote table "workouts" */
export type Workouts_Obj_Rel_Insert_Input = {
  data: Workouts_Insert_Input;
  on_conflict?: Maybe<Workouts_On_Conflict>;
};

/** on conflict condition type for table "workouts" */
export type Workouts_On_Conflict = {
  constraint: Workouts_Constraint;
  update_columns: Array<Workouts_Update_Column>;
  where?: Maybe<Workouts_Bool_Exp>;
};

/** ordering options when selecting data from "workouts" */
export type Workouts_Order_By = {
  asset?: Maybe<Assets_Order_By>;
  assets_aggregate?: Maybe<Assets_Aggregate_Order_By>;
  description?: Maybe<Order_By>;
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  image_asset_id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  round?: Maybe<Rounds_Order_By>;
  round_id?: Maybe<Order_By>;
  rounds_aggregate?: Maybe<Rounds_Aggregate_Order_By>;
};

/** primary key columns input for table: "workouts" */
export type Workouts_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "workouts" */
export enum Workouts_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Difficulty = 'difficulty',
  /** column name */
  Duration = 'duration',
  /** column name */
  Id = 'id',
  /** column name */
  ImageAssetId = 'image_asset_id',
  /** column name */
  Name = 'name',
  /** column name */
  RoundId = 'round_id'
}

/** input type for updating data in table "workouts" */
export type Workouts_Set_Input = {
  description?: Maybe<Scalars['String']>;
  difficulty?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['uuid']>;
  image_asset_id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  round_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type Workouts_Stddev_Fields = {
  __typename?: 'workouts_stddev_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "workouts" */
export type Workouts_Stddev_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Workouts_Stddev_Pop_Fields = {
  __typename?: 'workouts_stddev_pop_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "workouts" */
export type Workouts_Stddev_Pop_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Workouts_Stddev_Samp_Fields = {
  __typename?: 'workouts_stddev_samp_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "workouts" */
export type Workouts_Stddev_Samp_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Workouts_Sum_Fields = {
  __typename?: 'workouts_sum_fields';
  difficulty?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "workouts" */
export type Workouts_Sum_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** update columns of table "workouts" */
export enum Workouts_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Difficulty = 'difficulty',
  /** column name */
  Duration = 'duration',
  /** column name */
  Id = 'id',
  /** column name */
  ImageAssetId = 'image_asset_id',
  /** column name */
  Name = 'name',
  /** column name */
  RoundId = 'round_id'
}

/** aggregate var_pop on columns */
export type Workouts_Var_Pop_Fields = {
  __typename?: 'workouts_var_pop_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "workouts" */
export type Workouts_Var_Pop_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Workouts_Var_Samp_Fields = {
  __typename?: 'workouts_var_samp_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "workouts" */
export type Workouts_Var_Samp_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Workouts_Variance_Fields = {
  __typename?: 'workouts_variance_fields';
  difficulty?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "workouts" */
export type Workouts_Variance_Order_By = {
  difficulty?: Maybe<Order_By>;
  duration?: Maybe<Order_By>;
};

import {
  Action,
  createAction,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { RecursivePartial } from "../types/global";
import {
  Exercise_Types_Enum,
  Round_Exercises,
  Rounds,
  Workouts,
} from "../generated/graphql";
import { v4 as uuid } from "uuid";

export const reorderRounds = createAction("reorderRounds");
export const reorderRoundExercises = createAction<number>(
  "reorderRoundExercises"
);

const workoutFormSlice = createSlice({
  extraReducers: (builder) => {
    builder.addCase(reorderRounds, (state, action) => {
      state.rounds = state.rounds.map((item, index) => {
        return {
          ...item,
          order: index,
        };
      });
    });
    builder.addCase(reorderRoundExercises, (state, action) => {
      state.rounds[action.payload].round_exercises = state.rounds[
        action.payload
      ].round_exercises.map((item, index) => {
        return {
          ...item,
          order: index,
        };
      });
    });
  },
  reducers: {
    setRounds: (state, action: PayloadAction<RecursivePartial<Rounds[]>>) => {
      state.rounds = action.payload;
    },
    setRoundExercises: (
      state,
      action: PayloadAction<{
        roundIndex: number;
        roundExercises: RecursivePartial<Round_Exercises[]>;
      }>
    ) => {
      state.rounds[action.payload.roundIndex].round_exercises =
        action.payload.roundExercises;
    },
    removeRoundExercise: (
      state,
      action: PayloadAction<{
        roundIndex: number;
        roundExerciseIndex: number;
      }>
    ) => {
      state.rounds[action.payload.roundIndex].round_exercises.splice(
        action.payload.roundExerciseIndex,
        1
      );
    },
    addRoundExercise: {
      prepare: (roundIndex: number) => {
        return {
          payload: {
            roundIndex,
            roundExercise: {
              id: uuid(),
              type: Exercise_Types_Enum.Repetition,
              value: 10,
              exercise: {
                name: "",
              },
            },
          },
        };
      },
      reducer: (
        state,
        action: PayloadAction<{
          roundIndex: number;
          roundExercise: RecursivePartial<Round_Exercises>;
        }>
      ) => {
        state.rounds[action.payload.roundIndex].round_exercises.push(
          action.payload.roundExercise
        );
      },
    },
    removeRound: (state, action: PayloadAction<number>) => {
      state.rounds.splice(action.payload, 1);
    },
    addRound: {
      prepare: () => {
        return {
          payload: {
            id: uuid(),
            round_exercises: [],
          } as RecursivePartial<Rounds>,
        };
      },
      reducer: (state, action: PayloadAction<RecursivePartial<Rounds>>) => {
        state.rounds.push(action.payload);
      },
    },
  },
  name: "workoutForm",
  initialState: {
    id: uuid(),
    name: "Workout A",
    description: "Workout A Description",
    difficulty: 2,
    duration: 3,
    asset: {
      url: "https://picsum.photos/200/200",
      type: "image/jpg",
    },
    rounds: [
      {
        order: 0,
        id: uuid(),
        round_exercises: [
          {
            order: 0,
            id: uuid(),
            value: 10,
            type: Exercise_Types_Enum.Repetition,
            exercise: {
              name: "Burpees",
            },
          },
          {
            id: uuid(),
            order: 1,
            value: 100,
            type: Exercise_Types_Enum.Repetition,
            exercise: {
              name: "Pushups",
            },
          },
        ],
      },
      {
        order: 1,
        id: uuid(),
        round_exercises: [],
      },
    ],
  } as RecursivePartial<Workouts>,
});

export const {
  addRound,
  removeRound,
  addRoundExercise,
  removeRoundExercise,
  setRounds,
  setRoundExercises,
} = workoutFormSlice.actions;
export default workoutFormSlice;

import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RecursivePartial } from "../types/global";
import { Rounds } from "../generated/graphql";
import { GlobalAppState } from "./index";

export const setLastRoundAsCurrentRound = createAsyncThunk(
  "setLastRoundAsCurrentRound",
  async (_, thunkAPI) => {
    const state = (await thunkAPI.getState()) as GlobalAppState;
    return {
      ...state.workoutForm.rounds[state.workoutForm.rounds.length - 1],
      order: state.workoutForm.rounds.length - 1,
    };
  }
);
export const currentRoundSlice = createSlice({
  name: "selectors/currentRound",
  extraReducers: (builder) => {
    builder.addCase(setLastRoundAsCurrentRound.fulfilled, (state, action) => {
      state.data = action.payload;
    });
  },
  reducers: {
    setCurrentRound: (
      state,
      action: PayloadAction<RecursivePartial<Rounds>>
    ) => {
      state.data = action.payload;
    },
    resetCurrentRound: (
      state,
      action: PayloadAction<RecursivePartial<Rounds>>
    ) => {
      state.data = undefined;
    },
  },
  initialState: {
    data: undefined as RecursivePartial<Rounds>,
  },
});

export const { setCurrentRound, resetCurrentRound } = currentRoundSlice.actions;

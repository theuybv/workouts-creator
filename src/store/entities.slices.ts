import {
  createEntityAdapter,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { RecursivePartial } from "../types/global";
import { Round_Exercises, Rounds, Workouts } from "../generated/graphql";

export const workoutsAdapter = createEntityAdapter<RecursivePartial<Workouts>>({
  // Assume IDs are stored in a field other than `book.id`
  selectId: ({ id }) => id,
  // Keep the "all IDs" array sorted based on book titles
  // sortComparer: (a, b) => a.title.localeCompare(b.title),
});

export const roundsAdapter = createEntityAdapter<RecursivePartial<Rounds>>({
  // Assume IDs are stored in a field other than `book.id`
  selectId: ({ id }) => id,
  // Keep the "all IDs" array sorted based on book titles
  // sortComparer: (a, b) => a.title.localeCompare(b.title),
});

export const roundExercisesAdapter = createEntityAdapter<
  RecursivePartial<Round_Exercises>
>({
  // Assume IDs are stored in a field other than `book.id`
  selectId: ({ id }) => id,
  // Keep the "all IDs" array sorted based on book titles
  // sortComparer: (a, b) => a.title.localeCompare(b.title),
});

export const workoutsSlice = createSlice({
  name: "workouts",
  initialState: workoutsAdapter.getInitialState(),
  reducers: {
    // Can pass adapter functions directly as case reducers.  Because we're passing this
    // as a value, `createSlice` will auto-generate the `bookAdded` action type / creator
    addOne: workoutsAdapter.addOne,
    updateOne: workoutsAdapter.updateOne,
    addRound: (
      state,
      action: PayloadAction<{ id: string; round: RecursivePartial<Rounds> }>
    ) => {
      state.entities[action.payload.id].rounds.push(action.payload.round);
    },
    removeRound: (
      state,
      action: PayloadAction<{
        id: string;
        roundIndex: number;
      }>
    ) => {
      state.entities[action.payload.id].rounds.splice(
        action.payload.roundIndex,
        1
      );
    },
    addRoundExercise: (
      state,
      action: PayloadAction<{
        id: string;
        roundIndex: number;
        roundExercise: RecursivePartial<Round_Exercises>;
      }>
    ) => {
      state.entities[action.payload.id].rounds[
        action.payload.roundIndex
      ].round_exercises.push(action.payload.roundExercise);
    },
    removeRoundExercise: (
      state,
      action: PayloadAction<{
        id: string;
        roundIndex: number;
        roundExerciseIndex: number;
      }>
    ) => {
      state.entities[action.payload.id].rounds[
        action.payload.roundIndex
      ].round_exercises.splice(action.payload.roundExerciseIndex, 1);
    },
  },
});

export const roundsSlice = createSlice({
  name: "rounds",
  initialState: roundsAdapter.getInitialState(),
  reducers: {
    // Can pass adapter functions directly as case reducers.  Because we're passing this
    // as a value, `createSlice` will auto-generate the `bookAdded` action type / creator
    addOne: roundsAdapter.addOne,
  },
});

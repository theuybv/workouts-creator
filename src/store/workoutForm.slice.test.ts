import { createStore } from "@reduxjs/toolkit";
import workoutFormSlice, {
  addRound,
  addRoundExercise,
  removeRound,
  reorderRoundExercises,
  reorderRounds,
} from "./workoutForm.slice";
import { Exercise_Types_Enum } from "../generated/graphql";

let workoutFormSliceTest: any;
beforeEach(() => {
  workoutFormSliceTest = createStore(workoutFormSlice.reducer);
});

test("initial state", () => {
  const state = workoutFormSliceTest.getState();
  expect(typeof state.id).toBe("string");
  expect(state.rounds).toHaveLength(0);
});

test("add round", () => {
  workoutFormSliceTest.dispatch(addRound());
  const state = workoutFormSliceTest.getState();
  expect(state.rounds).toHaveLength(1);
  expect(state.rounds[0].round_exercises).toHaveLength(0);
  expect(state.rounds[0].order).toBeUndefined();
});

test("remove round", () => {
  workoutFormSliceTest.dispatch(removeRound(0));
  const state = workoutFormSliceTest.getState();
  expect(state.rounds).toHaveLength(0);
});

test("add multiple round and reorder", () => {
  let state;
  workoutFormSliceTest.dispatch(addRound());
  workoutFormSliceTest.dispatch(addRound());
  workoutFormSliceTest.dispatch(addRound());
  workoutFormSliceTest.dispatch(addRound());
  workoutFormSliceTest.dispatch(addRound());
  state = workoutFormSliceTest.getState();
  expect(state.rounds).toHaveLength(5);
  workoutFormSliceTest.dispatch(reorderRounds());
  state = workoutFormSliceTest.getState();
  expect(state.rounds[0].order).toBe(0);
  expect(state.rounds[1].order).toBe(1);
});

test("add roundExercise to a round ", () => {
  let state;
  workoutFormSliceTest.dispatch(addRound());
  state = workoutFormSliceTest.getState();
  expect(state.rounds[0].round_exercises).toHaveLength(0);
  workoutFormSliceTest.dispatch(addRoundExercise(0));
  state = workoutFormSliceTest.getState();
  expect(state.rounds[0].round_exercises).toHaveLength(1);
  expect(state.rounds[0].round_exercises[0].order).toBeUndefined();
  expect(typeof state.rounds[0].round_exercises[0].id).toBe("string");
  expect(state.rounds[0].round_exercises[0].type).toBe(
    Exercise_Types_Enum.Repetition
  );
  expect(state.rounds[0].round_exercises[0].exercise.name).toBe("");
  expect(state.rounds[0].round_exercises[0].value).toBe(10);
});

test("add multiple roundExercises and reorder", () => {
  let state;
  workoutFormSliceTest.dispatch(addRound());
  workoutFormSliceTest.dispatch(addRoundExercise(0));
  workoutFormSliceTest.dispatch(reorderRoundExercises(0));
  state = workoutFormSliceTest.getState();
  expect(state.rounds[0].round_exercises[0].order).toBe(0);
});

import { createStore } from "@reduxjs/toolkit";
import { workoutsAdapter, workoutsSlice } from "./entities.slices";
import { v4 as uuid } from "uuid";
import { RecursivePartial } from "../types/global";
import { Workouts } from "../generated/graphql";

const workoutId = "test-test-test";
let store: any;
const data: RecursivePartial<Workouts> = {
  id: workoutId,
  name: "Workout A",
  rounds: [],
};
beforeEach(() => {
  store = createStore(workoutsSlice.reducer);
  store.dispatch(workoutsSlice.actions.addOne(data));
});
test("add workout", () => {
  expect(store.getState()).toEqual({
    entities: {
      ["test-test-test"]: {
        id: "test-test-test",
        name: data.name,
        rounds: [],
      },
    },
    ids: ["test-test-test"],
  });
});

test("addRound to workout", () => {
  const selectors = workoutsAdapter.getSelectors();
  store.dispatch(
    workoutsSlice.actions.addRound({
      id: workoutId,
      round: {
        id: "round1",
        round_exercises: [],
      },
    })
  );
  expect(selectors.selectById(store.getState(), workoutId)).toEqual({
    id: "test-test-test",
    name: "Workout A",
    rounds: [{ id: "round1", round_exercises: [] }],
  });
});

test("remove round from workout", () => {
  store.dispatch(
    workoutsSlice.actions.addRound({
      id: workoutId,
      round: {
        id: "round1",
        round_exercises: [],
      },
    })
  );
  store.dispatch(
    workoutsSlice.actions.addRound({
      id: workoutId,
      round: {
        id: "round2",
        round_exercises: [],
      },
    })
  );
  store.dispatch(
    workoutsSlice.actions.removeRound({ id: workoutId, roundIndex: 0 })
  );
  expect(
    workoutsAdapter.getSelectors().selectById(store.getState(), workoutId)
  ).toEqual({
    id: "test-test-test",
    name: "Workout A",
    rounds: [{ id: "round2", round_exercises: [] }],
  });
});

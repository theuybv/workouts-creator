import { combineReducers, configureStore } from "@reduxjs/toolkit";
import workoutFormSlice from "./workoutForm.slice";
import { currentRoundSlice } from "./currentSelectors.slice";
import { reducer as formReducer } from "redux-form";

const reducer = combineReducers({
  workoutForm: workoutFormSlice.reducer,
  currentRound: currentRoundSlice.reducer,
  form: formReducer,
});

export type GlobalAppState = ReturnType<typeof reducer>;

export default configureStore({ reducer });

import React, { FC } from "react";
import { IonIcon, IonItem, IonText } from "@ionic/react";
import { closeCircle, pencilOutline } from "ionicons/icons";
import { RecursivePartial } from "../types/global";
import { Exercise_Types_Enum, Rounds } from "../generated/graphql";

const RoundItem: FC<{
  round: RecursivePartial<Rounds>;
  onItemClick?: (round: RecursivePartial<Rounds>) => void;
  onDeleteClick?: (round: RecursivePartial<Rounds>) => void;
}> = (props) => {
  const typeUnit = (type: Exercise_Types_Enum) => {
    switch (type) {
      case "DURATION":
        return "sec.";
      case "REPETITION":
        return "x";
      case "REST":
        return "sec.";
      default:
        break;
    }
  };
  return (
    <IonItem onClick={(e) => {}}>
      <p>
        <IonText>
          Round {props.round.order + 1} -{" "}
          {props.round.round_exercises
            ? props.round.round_exercises
                .map((item: any) => {
                  return `${item.exercise?.name} ${item.value}${typeUnit(
                    item.type
                  )}`;
                })
                .join(", ")
            : null}
        </IonText>
      </p>
      <IonIcon
        size={"small"}
        icon={pencilOutline}
        slot="end"
        onClick={(e) => {
          props.onItemClick && props.onItemClick(props.round);
        }}
      />
      <IonIcon
        icon={closeCircle}
        size={"small"}
        slot="end"
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          props.onDeleteClick && props.onDeleteClick(props.round);
        }}
      />

      {props.children}
    </IonItem>
  );
};

export default RoundItem;

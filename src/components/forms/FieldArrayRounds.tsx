import React, { FC, useState } from "react";
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonList,
  IonModal,
  IonReorder,
  IonReorderGroup,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { RecursivePartial } from "../../types/global";
import { Rounds } from "../../generated/graphql";
import {
  Field,
  FieldArray,
  WrappedFieldArrayProps,
  WrappedFieldProps,
} from "redux-form";
import RoundItem from "../RoundItem";
import { v4 } from "uuid";
import { add } from "ionicons/icons";
import FieldArrayRoundExercises from "./FieldArrayRoundExercises";

const FieldArrayRounds: FC<
  WrappedFieldArrayProps<RecursivePartial<Rounds>>
> = ({ fields, meta: { error, submitFailed } }) => {
  const [modalId, setModalId] = useState<string | unknown>();
  return (
    <>
      <div className="ion-padding">
        <IonText>
          <h3>
            {fields.getAll().length
              ? `${fields.length} rounds of exercises`
              : "There are no workout rounds"}
          </h3>
        </IonText>
      </div>
      <IonList>
        <IonReorderGroup
          disabled={false}
          onIonItemReorder={(event) => {
            const rounds: RecursivePartial<Rounds[]> = event.detail.complete([
              ...fields.getAll(),
            ]);
            fields.removeAll();
            rounds.forEach((item, index) => {
              fields.insert(index, {
                ...item,
                order: index,
              });
            });
          }}
        >
          {fields.map((item: string, index: number) => {
            return (
              <div key={index}>
                <Field
                  name={`${item}`}
                  component={({
                    input,
                    meta: { touched, error },
                  }: WrappedFieldProps) => {
                    return (
                      <RoundItem
                        round={input.value}
                        onDeleteClick={async (round) => {
                          fields.remove(index);
                        }}
                        onItemClick={(round) => {
                          setModalId(round.id);
                        }}
                      >
                        <IonReorder slot="end" />
                      </RoundItem>
                    );
                  }}
                />
                <IonModal isOpen={modalId === fields.getAll()[index].id}>
                  <IonHeader>
                    <IonToolbar>
                      <IonButtons>
                        <IonButton
                          slot={"end"}
                          onClick={(event) => {
                            setModalId(undefined);
                          }}
                        >
                          Save
                        </IonButton>
                      </IonButtons>
                      <IonTitle>Round {index + 1} exercises</IonTitle>
                    </IonToolbar>
                  </IonHeader>
                  <IonContent fullscreen>
                    <FieldArray
                      props={null}
                      name={`${item}.round_exercises`}
                      component={FieldArrayRoundExercises}
                    />
                  </IonContent>
                </IonModal>
              </div>
            );
          })}
        </IonReorderGroup>
        <div className="ion-padding">
          <IonButton
            size="small"
            onClick={async () => {
              const order = fields.getAll().length
                ? fields.getAll()[fields.length - 1].order + 1
                : 0;
              fields.push({
                id: v4(),
                order,
                round_exercises: [],
              });
            }}
          >
            <IonIcon slot="start" icon={add} />
            add a round
          </IonButton>
        </div>
      </IonList>
    </>
  );
};

export default FieldArrayRounds;

import React, { FC } from "react";
import { IonLabel, IonSegment, IonSegmentButton } from "@ionic/react";
import { WrappedFieldProps } from "redux-form";
import { ListInputProps } from "./FieldIonSelect";

const FieldIonSegment: FC<WrappedFieldProps & ListInputProps> = ({
  input,
  label,
  meta: { touched, error, warning },
  options,
}) => (
  <>
    <IonLabel className="ion-padding-bottom" position="stacked">
      {label}
    </IonLabel>
    <IonSegment onIonChange={input.onChange} value={String(input.value)}>
      {options.map((item: any, index: number) => {
        return (
          <IonSegmentButton key={index} value={String(item.value)}>
            <IonLabel>{item.label}</IonLabel>
          </IonSegmentButton>
        );
      })}
    </IonSegment>
  </>
);

export default FieldIonSegment;

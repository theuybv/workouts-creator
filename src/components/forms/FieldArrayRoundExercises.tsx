import React, { FC } from "react";
import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCol,
  IonItem,
  IonList,
  IonReorder,
  IonReorderGroup,
  IonRow,
} from "@ionic/react";
import { RecursivePartial } from "../../types/global";
import { Exercise_Types_Enum, Round_Exercises } from "../../generated/graphql";
import { Field, WrappedFieldArrayProps } from "redux-form";
import { v4 } from "uuid";
import FieldIonInput from "./FieldIonInput";
import FieldIonSelect from "./FieldIonSelect";

const FieldArrayRoundExercises: FC<
  WrappedFieldArrayProps<RecursivePartial<Round_Exercises>>
> = ({ fields }) => {
  return (
    <>
      <IonList>
        <IonReorderGroup
          disabled={false}
          onIonItemReorder={(event) => {
            const roundExercises: RecursivePartial<
              Round_Exercises[]
            > = event.detail.complete([...fields.getAll()]);
            fields.removeAll();
            roundExercises.forEach((item, index) => {
              fields.insert(index, {
                ...item,
                order: index,
              });
            });
          }}
        >
          {fields.map((item: string, index: number) => {
            return (
              <IonCard key={index}>
                <IonCardHeader>
                  <IonItem>
                    <IonCardSubtitle>Exercise {index + 1}</IonCardSubtitle>
                    <IonReorder slot="end" />
                  </IonItem>
                </IonCardHeader>
                <IonCardContent>
                  <IonList>
                    <IonItem>
                      <Field
                        name={`${item}.exercise.name`}
                        type={"text"}
                        label={"Exercise name"}
                        component={FieldIonInput}
                      />
                    </IonItem>
                  </IonList>
                  <IonList>
                    <IonItem>
                      <Field
                        name={`${item}.type`}
                        label={"Exercise Type"}
                        options={[
                          {
                            label: Exercise_Types_Enum.Repetition.toString(),
                            value: Exercise_Types_Enum.Repetition,
                          },
                          {
                            label: Exercise_Types_Enum.Duration.toString(),
                            value: Exercise_Types_Enum.Duration,
                          },
                          {
                            label: Exercise_Types_Enum.Rest.toString(),
                            value: Exercise_Types_Enum.Rest,
                          },
                        ]}
                        component={FieldIonSelect}
                      />
                    </IonItem>
                  </IonList>
                  <IonList>
                    <IonItem>
                      <Field
                        name={`${item}.value`}
                        type={"number"}
                        label={
                          fields.get(index).type ===
                          Exercise_Types_Enum.Repetition
                            ? "How many repetitions?"
                            : fields.get(index).type ===
                              Exercise_Types_Enum.Duration
                            ? "How long does the exercise take? (sec.)"
                            : "How long will the break last? (sec.)"
                        }
                        component={FieldIonInput}
                      />
                    </IonItem>
                  </IonList>
                </IonCardContent>
              </IonCard>
            );
          })}
        </IonReorderGroup>
        <IonRow>
          <IonCol>
            <IonButton
              fill="clear"
              expand="full"
              size="small"
              onClick={() => {
                const order = fields.getAll().length
                  ? fields.getAll()[fields.length - 1].order + 1
                  : 0;
                fields.push({
                  id: v4(),
                  type: Exercise_Types_Enum.Repetition,
                  value: 10,
                  order,
                  exercise: {
                    name: "Burpees",
                  },
                });
              }}
            >
              ADD EXERCISE
            </IonButton>
          </IonCol>
        </IonRow>
      </IonList>
    </>
  );
};

export default FieldArrayRoundExercises;

import React, { FC } from "react";
import { IonInput, IonLabel } from "@ionic/react";
import { WrappedFieldProps } from "redux-form";

const FieldIonInput: FC<WrappedFieldProps & { label: string }> = ({
  input,
  label,
  meta: { touched, error, warning },
}) => (
  <>
    <IonLabel position="stacked">{label}</IonLabel>
    <IonInput value={input.value} onIonChange={input.onChange} />
  </>
);
export default FieldIonInput;

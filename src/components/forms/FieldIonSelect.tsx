import React, { FC } from "react";
import {
  IonItem,
  IonLabel,
  IonSegment,
  IonSegmentButton,
  IonSelect,
  IonSelectOption,
} from "@ionic/react";
import { BaseFieldProps, WrappedFieldProps } from "redux-form";

export type ListInputProps = {
  label: string;
  placeholder: string;
  options: { label: string; value: string }[];
};
const FieldIonSelect: FC<WrappedFieldProps & ListInputProps> = ({
  input,
  meta: { touched, error, warning },
  placeholder,
  label,
  options,
}) => {
  return (
    <>
      <IonLabel position="stacked">{label}</IonLabel>
      <IonSelect
        onIonChange={input.onChange}
        value={String(input.value)}
        placeholder={placeholder}
      >
        {options.map((item: any, index: number) => {
          return (
            <IonSelectOption key={index} value={String(item.value)}>
              {item.label}
            </IonSelectOption>
          );
        })}
      </IonSelect>
    </>
  );
};

export default FieldIonSelect;

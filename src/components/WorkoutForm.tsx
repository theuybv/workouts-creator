import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonItem,
  IonLabel,
  IonList,
  IonRow,
} from "@ionic/react";
import React, { FC } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { RecursivePartial } from "../types/global";
import { Rounds, Workouts } from "../generated/graphql";
import { setCurrentRound } from "../store/currentSelectors.slice";
import { Field, FieldArray, InjectedFormProps, reduxForm } from "redux-form";
import FieldIonInput from "./forms/FieldIonInput";
import FieldIonSegment from "./forms/FieldIonSegment";
import FieldArrayRounds from "./forms/FieldArrayRounds";

const WorkoutForm: FC<InjectedFormProps<RecursivePartial<Workouts>>> = ({
  handleSubmit,
}) => {
  const state = useSelector((state) => state);
  const workoutState = state.workoutForm;

  const onSubmit = handleSubmit((values) => {
    console.log("onsubmit workout=====");
    console.log(values);
  });

  const dispatch = useDispatch();

  return (
    <>
      <IonContent fullscreen>
        <form onSubmit={onSubmit}>
          <IonList>
            <IonItem>
              <Field
                name="name"
                type={"text"}
                label={"Name"}
                component={FieldIonInput}
              />
            </IonItem>
            <IonItem>
              <Field
                name="description"
                type={"text"}
                label={"Description"}
                component={FieldIonInput}
              />
            </IonItem>
            <IonItem>
              <IonGrid>
                <IonRow>
                  <IonCol className="ion-padding-top ion-padding-bottom">
                    <Field
                      name="duration"
                      label={"Duration"}
                      options={[
                        { label: "Short", value: 1 },
                        { label: "Medium", value: 2 },
                        { label: "Long", value: 3 },
                      ]}
                      component={FieldIonSegment}
                    />
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>
            <IonItem>
              <IonGrid>
                <IonRow>
                  <IonCol className="ion-padding-top ion-padding-bottom">
                    <Field
                      name="difficulty"
                      label={"Difficulty"}
                      options={[
                        { label: "Easy", value: 1 },
                        { label: "Medium", value: 2 },
                        { label: "Hard", value: 3 },
                      ]}
                      component={FieldIonSegment}
                    />
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>
            <IonItem>
              <IonLabel position="stacked">Image</IonLabel>
              {workoutState.asset ? (
                <img
                  src={workoutState.asset.url}
                  alt={"image"}
                  width={"100%"}
                  style={{ maxHeight: 200 }}
                />
              ) : (
                <input type="file" name="file" />
              )}
            </IonItem>
          </IonList>
          <FieldArray
            name={"rounds"}
            component={FieldArrayRounds}
            onItemClick={(round: RecursivePartial<Rounds>) => {
              dispatch(setCurrentRound(round));
            }}
          />
        </form>
      </IonContent>
      <IonButton type={"button"} expand={"full"} onClick={onSubmit}>
        Save Round
      </IonButton>
    </>
  );
};

export default connect((state) => ({
  initialValues: state.workoutForm, // pull initial values from account reducer
}))(
  reduxForm({
    form: "workout",
  })(WorkoutForm)
);
